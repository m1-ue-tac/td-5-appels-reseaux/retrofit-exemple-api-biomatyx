package com.univlille.retrofitrxapig_fortin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.univlille.retrofitrxapig_fortin.api.API_Client;
import com.univlille.retrofitrxapig_fortin.model.productinfo.ProductInfo;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ProductInfoActivity extends AppCompatActivity {


    private TextView productName;
    private TextView productDescription;
    private Disposable resultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        productDescription = findViewById(R.id.productDescription);
        productName = findViewById(R.id.productName);
        TextView productRef = findViewById(R.id.productRef);
        ImageView productImage = findViewById(R.id.productImage);

        Intent intent = getIntent();

        String referenceProduct = intent.getStringExtra("refProduct");
        productRef.setText(referenceProduct);

        String urlImageProduct = intent.getStringExtra("urlImageProduct");
        Glide.with(this)
                .load(urlImageProduct)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(productImage);

        appelDetailProduct(referenceProduct);

        // TODO : améliorer l'IHM, afficher d'autres infos, prévoir une vue scrollable....

    }

    private void appelDetailProduct(String referenceProduct) {

        // on appelle l'API pour le produit passé en référence
        // https://biomatyx.fr/api/product?refNumber=referenceProduct
        Single<ProductInfo> productInfo = API_Client.getInstance().getMyApi().getProductInfo(referenceProduct)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        resultat = productInfo.subscribeWith(new DisposableSingleObserver<ProductInfo>() {
                                             @Override
                                             public void onSuccess(@NonNull ProductInfo productInfo) {
                                                productName.setText(productInfo.getName().getFr());
                                                productDescription.setText(productInfo.getDescription().getFr());
                                             }

                                             @Override
                                             public void onError(@NonNull Throwable e) {
                                                 Log.d("JC", "erreur : " + e.getMessage());
                                             }
                                         }


        );

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultat.dispose();
    }
}