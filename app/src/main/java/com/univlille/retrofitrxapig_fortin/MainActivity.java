package com.univlille.retrofitrxapig_fortin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.univlille.retrofitrxapig_fortin.api.API_Client;
import com.univlille.retrofitrxapig_fortin.model.catalog.Catalog;
import com.univlille.retrofitrxapig_fortin.model.catalog.Product;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements DisplayProduct {

    private RecyclerAdapter recyclerAdapter;
    private DisposableSingleObserver<Catalog> resultat;
    private int pageDebut;

    // TODO : afficher une toolbar, gérer les boutons pour ne pas arriver à numPage = 0 et négatif...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Product> productList = new ArrayList<>();
        pageDebut = 1;

        Button btn_previous = (Button) findViewById(R.id.btn_previous);
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageDebut--;
                appelCatalogue(pageDebut, 50);
            }
        });
        Button btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageDebut++;
                appelCatalogue(pageDebut, 50);
            }
        });

        // déclarations pour le recyclerview (dont la liste de followers comme data à afficher)
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(this, productList);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setHasFixedSize(true);

        // affiche catalogue produit avec la requête https://biomatyx.fr/api/catalog?page=1&pagesize=2
        appelCatalogue(1, 50);
//      appelCatalogue(2, 100);
    }

    private void appelCatalogue(int numPage, int nbProducts) {

        /**
         * on appelle l'API sur la page 1 avec 50 produits (por remplir sffisamment le RecyclerView plus tard)
         */

        Single<Catalog> catalog = API_Client.getInstance().getMyApi().getProductList(numPage, nbProducts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        resultat = catalog.subscribeWith(new DisposableSingleObserver<Catalog>() {
                                             @Override
                                             public void onSuccess(@NonNull Catalog catalog) {
                                                 recyclerAdapter.setProductList(catalog.productList);
                                             }

                                             @Override
                                             public void onError(@NonNull Throwable e) {
                                                 Log.d("JC", "erreur : " + e.getMessage());
                                             }
                                         }


        );

    }

    @Override
    public void displayProduct(String refProduct, String url) {
        Intent intent = new Intent(this, ProductInfoActivity.class);
        intent.putExtra("refProduct", refProduct);
        // comme l'image du produit resdte la même, on passe son URL pour éviter de la récupérer avec Rx dans l'activité détail du produit
        intent.putExtra("urlImageProduct", url);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultat.dispose();
    }

}