package com.univlille.retrofitrxapig_fortin.model.catalog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryFilter {

    @SerializedName("referenceNumber")
    @Expose
    public String referenceNumber;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("productCount")
    @Expose
    public Integer productCount;

}
