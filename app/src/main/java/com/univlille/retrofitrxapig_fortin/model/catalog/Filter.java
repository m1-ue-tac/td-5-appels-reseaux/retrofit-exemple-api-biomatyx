package com.univlille.retrofitrxapig_fortin.model.catalog;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filter {

    @SerializedName("categoryFilterList")
    @Expose
    public List<CategoryFilter> categoryFilterList = null;

    @SerializedName("productNameList")
    @Expose
    public List<String> productNameList = null;

}
