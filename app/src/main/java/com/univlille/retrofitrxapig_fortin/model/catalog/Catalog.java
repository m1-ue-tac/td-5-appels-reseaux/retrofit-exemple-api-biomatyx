package com.univlille.retrofitrxapig_fortin.model.catalog;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Catalog {

    @SerializedName("currentPage")
    @Expose
    public Integer currentPage;

    @SerializedName("pageSize")
    @Expose
    public Integer pageSize;

    @SerializedName("totalRecords")
    @Expose
    public Integer totalRecords;

    @SerializedName("productList")
    @Expose
    public List<Product> productList = null;

    @SerializedName("filter")
    @Expose
    public Filter filter;

}
