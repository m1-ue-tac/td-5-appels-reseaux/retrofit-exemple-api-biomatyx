package com.univlille.retrofitrxapig_fortin.model.catalog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("referenceNumber")
    @Expose
    public String referenceNumber;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("categoryRefNumber")
    @Expose
    public String categoryRefNumber;

    @SerializedName("manufacturerRefNumber")
    @Expose
    public String manufacturerRefNumber;

    @SerializedName("manufacturerName")
    @Expose
    public String manufacturerName;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("newProduct")
    @Expose
    public Boolean newProduct;

    @SerializedName("picture")
    @Expose
    public Picture picture;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getName() {
        return name;
    }

    public Picture getPicture() {
        return picture;
    }

    // Ici on permet à l'utilisateur de mettre en favori des data. Ce booléen N'EST PAS issu de l'API
    // Aussi, soit on ne stocke pas cette propriété (c'est le cas dans cet exemple), soit on
    // stocke cette propriété en local dans une BDD Room par exemple (ou par un appel d'API)
    // Cependant, cette donnée doit rester correcte pendant les scrolls sur une page. Par contre,
    // quand on change de page, on appelle l'API et donc la propriété est réinitialisée !
    public Boolean favori = false;
}
