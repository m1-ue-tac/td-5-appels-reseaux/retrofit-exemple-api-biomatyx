package com.univlille.retrofitrxapig_fortin.model.productinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProductInfo {

    @SerializedName("name")
    @Expose
    public Name name;

    @SerializedName("description")
    @Expose
    public Description description;

    public Name getName() {
        return name;
    }

    public Description getDescription() {
        return description;
    }
}
