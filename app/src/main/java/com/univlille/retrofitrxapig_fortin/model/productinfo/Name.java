package com.univlille.retrofitrxapig_fortin.model.productinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Name {

    @SerializedName("FR")
    @Expose
    public String fr;

    public String getFr() {
        return fr;
    }

}
