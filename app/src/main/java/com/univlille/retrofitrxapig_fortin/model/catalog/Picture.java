package com.univlille.retrofitrxapig_fortin.model.catalog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Picture {

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("tag")
    @Expose
    public String tag;

    public String getUrl() {
        return url;
    }
}
