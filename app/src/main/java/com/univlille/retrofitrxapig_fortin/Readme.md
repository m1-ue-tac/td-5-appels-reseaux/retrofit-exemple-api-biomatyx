# Appel de l'API du site https://biomatyx.fr/
_(grâcieusement fournie par Guillaume Fortin)_

Dans ce projet, nous appellerons l'API et nous afficherons les informations dans un RecyclerView.

## Pour commencer

Pensez à bien mettre à jour votre gradle avec les dépendances nécessaires pour Rx, Retofit, Glide...

### Pré-requis

- Cours et TD sur les appels réseaux
- Cours et TD sur le RecyclerView
- connaître les bases d'Android

# Fonctionnement de l'API
- Site: https://biomatyx.fr/
- API catalog: https://biomatyx.fr/api/catalog?page=1&pagesize=500
- API produit: https://biomatyx.fr/api/product?refNumber=FA-671, FA-671 correspond à l'url d'un produit
- Image produit: https://biomatyx.fr/images/product/thumbnail/FA-666%20(1).jpg
- Attention au piège des 'espaces' dans l’URL, de même que ()…
- L'url de l'image d'un produit = https://biomatyx.fr/images/product/thumbnail/ + url qui se trouve dans chaque ligne produit -> "picture":{"url":"FA-666 (1).jpg","tag":""}
