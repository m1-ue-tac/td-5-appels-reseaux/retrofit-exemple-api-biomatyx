package com.univlille.retrofitrxapig_fortin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.univlille.retrofitrxapig_fortin.model.catalog.Product;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyviewHolder> {

    private List<Product> productList;
    private Context context;
    private DisplayProduct displayProductInterface;

    private MyviewHolder holder;
    private int position;

    public RecyclerAdapter(Context context, List<Product> productList) {
        this.productList = productList;
        this.context = context;

        if (context instanceof DisplayProduct) {
            this.displayProductInterface = (DisplayProduct) context;
        }

    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new MyviewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.refProduct.setText(productList.get(position).getReferenceNumber());
        holder.nameProduct.setText(productList.get(position).getName());
        Glide.with(context)
                .load(URLimage(productList.get(position).getPicture().getUrl()))
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.imageProduct);

        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayProductInterface.displayProduct(productList.get(position).getReferenceNumber(), URLimage(productList.get(position).getPicture().getUrl()));
            }
        });

        // gestion du bouton switch qui simule la mise en favori
        // au lacement de l'appli, les booléens sont tous à faux (choix arbitraire)
        // Au cas où ils n'auraient pas été définis, on les met à faux.
        if (productList.get(position).favori == null) productList.get(position).favori = false;
        // on met le switch dans la position associée à ce booléen
        holder.btn_fav.setChecked(productList.get(position).favori);
        if (holder.btn_fav.isChecked())
            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.JC));
        else
            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        // quand on clique sur le switch, on met à jour la propriété du Product (valable uniquement pendant la session)
        holder.btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productList.get(position).favori = !productList.get(position).favori;
                if (holder.btn_fav.isChecked())
                    holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.JC));
                else
                    holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }
        });

    }

    private String URLimage(String url) {
        // comme certaines images ont un nom du type "FA-661 (1).jpg", il faut encoder
        // proprement l'URL : https://biomatyx.fr/images/product/thumbnail/FA-661%20(1).jpg
        // sinon on aura des ULs incomplètes : https://biomatyx.fr/images/product/thumbnail/FA-661 (1).jpg
        String URLimage = url;
        try {
            URLimage = URLEncoder.encode(URLimage, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        URLimage = URLimage.replace("+", "%20");
        String urlOK = "https://biomatyx.fr/images/product/thumbnail/" + URLimage;
        return urlOK;
    }


    @Override
    public int getItemCount() {
        if (productList != null) {
            return productList.size();
        }
        return 0;
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        private TextView refProduct;
        private TextView nameProduct;
        private ImageView imageProduct;
        private View view;

        private Switch btn_fav;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            refProduct = (TextView) itemView.findViewById(R.id.refProduct);
            nameProduct = (TextView) itemView.findViewById(R.id.nameProduct);
            refProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TOOO : afficher fiche produit

                }
            });
            imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);

            btn_fav = (Switch) itemView.findViewById(R.id.btn_favori);
            view = itemView;
        }

        public View getView() {
            return view;
        }
    }
}
