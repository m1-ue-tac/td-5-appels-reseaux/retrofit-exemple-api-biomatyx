package com.univlille.retrofitrxapig_fortin.api;

import com.univlille.retrofitrxapig_fortin.model.catalog.Catalog;
import com.univlille.retrofitrxapig_fortin.model.productinfo.ProductInfo;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API_Interface {

    //    https://biomatyx.fr/api/catalog?page=1&pagesize=2
    @GET("catalog")
    Single<Catalog> getProductList(@Query("page") int num_page, @Query("pagesize") int page_size);

    //    https://biomatyx.fr/api/product?refNumber=FA-671
    @GET("product")
    Single<ProductInfo> getProductInfo(@Query("refNumber") String productId);

}
