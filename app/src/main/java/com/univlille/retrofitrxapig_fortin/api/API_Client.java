package com.univlille.retrofitrxapig_fortin.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class API_Client {

    public static String BASE_URL =" https://biomatyx.fr/api/";

    private static Retrofit retrofit;

    private static API_Client instance = null;

    private API_Interface myAPI;

    public API_Client() {
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            myAPI = retrofit.create(API_Interface.class);
        }
    }

    public static API_Client getInstance() {
        if (instance == null) {
            instance = new API_Client();
        }
        return instance;
    }

    public API_Interface getMyApi() {
        return myAPI;
    }
}
