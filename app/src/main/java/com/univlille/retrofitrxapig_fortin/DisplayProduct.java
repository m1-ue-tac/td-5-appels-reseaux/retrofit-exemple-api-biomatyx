package com.univlille.retrofitrxapig_fortin;

public interface DisplayProduct {

    void displayProduct(String refProduct, String url);
}
